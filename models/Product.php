<?php
class Product
{
    private \PDO $conn;

    public function __construct(\PDO $conn)
    {
        $this->conn = $conn;
    }

    // CREATE
    public function create() {

    }

    public function showFilter($minPrice = null, $maxPrice = null, $name = null, $hasBtw = null) {
        $btw = 1.21;
        $query='select * from product';
        $whereUsed = false;

//        var_dump($minPrice);
//        var_dump($maxPrice);
//        var_dump($name);die;

        if ($hasBtw) {
            if ($minPrice !== null) {
                $query = $query . " WHERE price*'$btw' >= :minPrice";
                $whereUsed = true;
            }

            if ($maxPrice !== null) {
                if ($whereUsed) {
                    $query = $query . " AND price*'$btw' <= :maxPrice";
                } else {
                    $query = $query . " WHERE price*'$btw' <= :maxPrice";
                }

                $whereUsed = true;
            }
        } else {
            if ($minPrice !== null) {
                $query = $query . " WHERE price >= :minPrice";
                $whereUsed = true;
            }

            if ($maxPrice !== null) {
                if ($whereUsed) {
                    $query = $query . " AND price <= :maxPrice";
                } else {
                    $query = $query . " WHERE price <= :maxPrice";
                }

                $whereUsed = true;
            }
        }

        if (!empty($name)) {
            if ($whereUsed) {
                $query = $query . " AND name LIKE :name";
            } else {
                $query = $query . " WHERE name LIKE :name";
            }
        }

        $connQuery = $this->conn->prepare($query);

        if ($minPrice !== null)
            $connQuery->bindParam(':minPrice', $minPrice, PDO::PARAM_STR);

        if ($maxPrice !== null)
            $connQuery->bindParam(':maxPrice', $maxPrice, PDO::PARAM_STR);

        if (!empty($name)) {
            $nameLike = '%' . $name . '%';
            $connQuery->bindParam(':name', $nameLike, PDO::PARAM_STR);
        }
        $connQuery->execute();
        return $connQuery;
    }



    // READ
    public function show() {
        return $this->conn->query('SELECT * FROM product');

    }
    // UPDATE
    public function edit() {

    }
    // DELETE
    public function destroy() {

    }

    public function getProductHistory($user_id) {
        $invoiceProductsSql = "SELECT i.invoice_id, p.name, cost, amount FROM invoiceProducts
        JOIN invoices i on invoiceProducts.invoice_id = i.invoice_id
        JOIN users u on i.user_id = u.user_id
        JOIN product p on invoiceProducts.product_id = p.product_id
        WHERE u.user_id = '$user_id'
        ORDER BY invoice_id DESC, i.createdAt DESC;";

        $invoiceProductsQuery = $this->conn->query($invoiceProductsSql);
        return $invoiceProductsQuery->fetchAll(PDO::FETCH_ASSOC);
    }
}