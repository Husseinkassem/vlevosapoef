<?php
class Invoice
{
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    // CREATE
    public function create() {

    }
    // READ
    public function show() {
        $invoicesSql = "SELECT * FROM invoices";

        $invoicesQuery = $this->conn->query($invoicesSql);
        return $invoicesQuery->fetchAll(PDO::FETCH_ASSOC);
    }
    // UPDATE
    public function edit() {

    }
    // DELETE
    public function destroy() {

    }

    public function showInvoicePerUser($id) {
        $invoicesSql = "SELECT * FROM invoices WHERE invoices.user_id = '$id'";

        $invoicesQuery = $this->conn->query($invoicesSql);
        return $invoicesQuery->fetchAll(PDO::FETCH_ASSOC);
    }
}