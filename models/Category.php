<?php

class Category
{
   protected $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    //CREATE
    public function create()
    {

    }

    //READ
    public function show()
    {
        $stmt = $this->conn->query('select * from category');
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    // UPDATE
    public function edit()
    {

    }

    // DELETE
    public function destroy()
    {

    }

}

