<?php

class Connection
{
    public static function make($config)
    {
        try {
            return new PDO($config['dsn'],
                $config['username'],
                $config['password'],
                $config['options']
            );
        }catch(Exception $e){
            die(var_dump($e ->getMessage()));
        }
    }
}
