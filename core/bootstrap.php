<?php
$app = [];
$app['config'] = require 'config.php';

/**
 * Ophalen classes
 */
require 'core/database/Connection.php';
require 'core/Routes.php';
require 'core/Request.php';

$app['database'] = Connection::make($app['config']['database']);