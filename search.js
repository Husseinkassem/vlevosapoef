function filterSearch() {
    $('.searchResult').html('<div id="loading">Loading .....</div>');
    var action = 'fetch_data';
    var minPrice = $('#minPrice').val();
    var maxPrice = $('#maxPrice').val();
    var name = getFilterData('name');
    var price = getFilterData('price');
    var description = getFilterData('description');
    $.ajax({
        url:"action.php",
        method:"POST",
        dataType: "json",
        data:{action:action, minPrice:	minPrice, maxPrice:maxPrice,
            name:name, price:price, description:description},
        success:function(data){
            $('.searchResult').html(data.html);
        }
    });
}