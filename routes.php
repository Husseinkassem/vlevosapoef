<?php
$router->define([
    '' => 'controllers/home.php',
    'products' => 'controllers/products.php',
    'contact' => 'controllers/contact.php',
    'faq' => 'controllers/faq.php',
    'register' => 'controllers/register.php',
    'login' => 'controllers/login.php',
    'logout' => 'controllers/logout.php',
    'reset-password' => 'controllers/reset-password.php',
    'product-history' => 'controllers/product-history.php',
    'edit-profile' => 'controllers/edit-profile.php',
    'add-product' => 'controllers/add-product.php',
    'winkelwagen' => 'controllers/winkelwagen.php',
    'edit-delete-product' => 'controllers/edit-delete-product.php',
    'edit-product' => 'controllers/edit-product.php',
    'delete-product' => 'controllers/delete-product.php',
    'register-private' => 'controllers/register-private.php',
    'profile'  => 'controllers/profile.php',
    'checkout' => 'controllers/checkout.php'
]);