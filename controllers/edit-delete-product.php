<?php
if ($_SESSION['isAdmin'] !== true) {
    header('location: /');
}

$pdo = Connection::make($app['config']['database']);
$model = new Product($pdo);
    $sql = $model->show();
    $data = $sql->fetchAll(PDO::FETCH_ASSOC);

    foreach ($data as $item) {
        $products[] = [
            'id' => $item['product_id'],
            'name' => $item['name'],
            'price' => $item['price'],
            'desc' => $item['description'],
            'image' => $item['image']
        ];
    }

require 'views/edit-delete-product.view.php';

