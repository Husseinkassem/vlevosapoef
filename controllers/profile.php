<?php

if (empty($_SESSION['loggedin'])) {
    // If they are not, redirect them to the login page.
    header("Location: login");

    // Remember that this die statement is absolutely critical.  Without it,
    // people can view your members-only content without logging in.
    die("Redirecting to login");
}

$pdo = Connection::make($app['config']['database']);
$id = $_SESSION["id"];
$sql = "SELECT * FROM users WHERE user_id = $id";

if ($stmt = $pdo->prepare($sql)) {
    // Attempt to execute the prepared statement
    if ($stmt->execute()) {
        // Check if username exists, if yes then verify password
        if ($stmt->rowCount() == 1) {
            if ($row = $stmt->fetch()) {
                $usernameValue = $row['username'];
                $firstnameValue = $row['first_name'];
                $lastnameValue = $row['last_name'];
                $streetNameValue = $row['streetName'];
                $homeNoValue = $row['homeNo'];
                $postcodeValue = $row['postcode'];
            }

            // Close statement
            unset($stmt);
        }

    }
}
require 'views/profile.view.php';