<?php
$faq = [
    [
    'q' => 'Moeten bedrijven ook btw betalen?',
    'a' => 'Nee, als je een account heb waar een btw-nummer aan gekoppelt staat hoef je geen btw te betalen.'
    ],
    [
        'q' => 'Wanneer heb ik mijn sappies binnen?',
        'a' => 'Binnen 1 tot 3 werkdagen.'
    ],
    [
        'q' => 'Is Flevosap vegatarisch?',
        'a' => 'Ja, in tegenstelling tot andere sappen, gebruiken wij geen vlees. En we testen niet op dieren, hoewel ze dat vast niet erg zouden vinden.'
    ],
    [
        'q' => 'Waar ligt Flevosap?',
        'a' => 'In flevoland natuurlijk, in Biddinghuizen om precies te zijn!'
    ],
    [
        'q' => 'Is het fruit vers?',
        'a' => 'Jazeker! Ons sap wordt elke dag 100% vers geperst en meteen gebottled!'
    ]
];
require 'views/faq.view.php';