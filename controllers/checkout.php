<?php

if (isset($_SESSION['id'])){
    //conn
    $pdo = Connection::make($app['config']['database']);

    $sql = "select postcode, streetName, homeNo from users where user_id = :user_id";

    //prep
    $stmt = $pdo->prepare($sql);

    //bind and set param
    $stmt->bindParam(":user_id", $param_user_id, PDO::PARAM_STR);
    $param_user_id = $_SESSION['id'];

    $stmt->execute();

    $results = $stmt->fetch();

}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['pay'])) {

        if (isset($_SESSION['id'])) {
            //make connection
            $pdo = Connection::make($app['config']['database']);

            //prep statement
            $sql = "INSERT INTO invoices (user_id, postcode, streetName, homeNo, totalPrice) VALUES (:user_id, :postcode, :steetname, :homeno, :totalprice);";

            //prepare
            $stmt = $pdo->prepare($sql);

            //Bind vars to parameters
            $stmt->bindParam(":user_id", $param_user_id, PDO::PARAM_STR);
            $stmt->bindParam(":postcode", $param_postcode, PDO::PARAM_STR);
            $stmt->bindParam(":steetname", $param_streetname, PDO::PARAM_STR);
            $stmt->bindParam(":homeno", $param_homeno, PDO::PARAM_STR);
            $stmt->bindParam(":totalprice", $param_totalprice, PDO::PARAM_STR);

            //set values of parameters
            $param_user_id = $_SESSION['id'];
            $param_postcode = trim($_POST["postcode"]);
            $param_streetname = trim($_POST["straat"]);
            $param_homeno = trim($_POST["homeno"]);
            $param_totalprice = trim($_POST["totalprice"]);

            //Execute
            $stmt->execute();

            //Get newest row in Invoices and copy the invoice id
            //prep sql
            $sql = "SELECT MAX(invoice_id) FROM invoices";

            //prepare
            $stmt = $pdo->prepare($sql);

            //Execute
            $stmt->execute();

            $results = $stmt->fetch();

            $invoice_id = $results[0];

            echo $invoice_id;


            //Add each product in $_SESSION['cart'] to invoiceproducts
            foreach ($_SESSION['cart'] as $cartrow) {
                //make sql statement
                $sql = "INSERT INTO invoiceProducts (invoice_id, product_id, amount, cost) VALUES (:invoice_id, :product_id, :amount, :cost)";

                //Prep
                $stmt = $pdo->prepare($sql);

                //Bind vars to parameters
                $stmt->bindParam(":invoice_id", $param_invoice_id, PDO::PARAM_STR);
                $stmt->bindParam(":product_id", $param_product_id, PDO::PARAM_STR);
                $stmt->bindParam(":amount", $param_amount, PDO::PARAM_STR);
                $stmt->bindParam(":cost", $param_cost, PDO::PARAM_STR);

                //set values of params
                $param_invoice_id = $invoice_id;
                $param_product_id = $cartrow['productId'];
                $param_amount = $cartrow['aantal'];
                $param_cost = $cartrow['price'];

                //Execute
                $stmt->execute();


            }

            //empty cart
            unset($_SESSION['cart']);
            $_SESSION['cart'] = [];
            header('location: product-history');
        }
        else{
            header('location: /');
            unset($_SESSION['cart']);
            $_SESSION['cart'] = [];
        }

    }

    if (isset($_POST['loadAdres'])) {

    }

}

require 'views/checkout.view.php';
