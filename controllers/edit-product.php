<?php
if ($_SESSION['isAdmin'] !== true) {
    header('location: /');
}

$pdo = Connection::make($app['config']['database']);
if(isset($_GET['updateid'])){
    $id = $_GET['updateid'];
}
$sql = "SELECT * FROM product WHERE product_id = $id";

if ($stmt = $pdo->prepare($sql)) {
    // Attempt to execute the prepared statement
    if ($stmt->execute()) {
        // Check if username exists, if yes then verify password
        if ($stmt->rowCount() == 1) {
            if ($row = $stmt->fetch()) {
                $prodname = $row['name'];
                $description = $row['description'];
                $prodprice = $row['price'];
            }

            // Close statement
            unset($stmt);
        }

    }
}

$errors = [];
$name = $prodname;
$price = $prodprice;
$desc = $description;
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $name   = $_POST['name']        ?? null;
    $price  = $_POST['price']       ?? null;
    $desc   = $_POST['description'] ?? null;
    if (empty($desc)) {
        $errors['description'] = 'Beschrijving is verplicht';
    }
    elseif (strlen($desc) > 65000) {
        $errors['description'] = 'Beschrijving is te lang';
    }

    if (empty($price)) {
        $errors['price'] = 'Prijs is verplicht';
    }
    elseif ($price < 0) {
        $errors['price'] = 'Prijs mag niet in de min';
    }
    elseif ($price > 100000) {
        $errors['price'] = 'Prijs mag niet hoger dan honderdduizend';
    }

    if (empty($name)) {
        $errors['name'] = 'Naam is verplicht';
    }
    elseif (strlen($name) > 30) {
        $errors['name'] = 'Naam mag niet langer dan 30 characters';
    }

    if (empty($errors)) {
        if(file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name'])) {
            require 'controllers/upload.php';
            $image = $_FILES['image']['name'];
            $sqlupdate = "UPDATE product SET name = '$name' , description= '$desc' , price= '$price', image= '$image' 
        WHERE product_id = $id";
        } else {
            $sqlupdate = "UPDATE product SET name = '$name' , description= '$desc' , price= '$price' 
        WHERE product_id = $id";
        }

        $stmt = $pdo->prepare($sqlupdate);
        // Attempt to execute the prepared statement
        $stmt->execute();

        // Close statement
        unset($stmt);

        header('Location: /edit-delete-product');
    }


}

require 'views/edit-product.view.php';
