<?php

if ($_SESSION['isAdmin'] !== true) {
    header('location: /');
}

$errors = [];
$name = '';
$price = 0;
$desc = '';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $pdo = Connection::make($app['config']['database']);
    $name   = $_POST['name']        ?? null;
    $price  = $_POST['price']       ?? null;
    $desc   = $_POST['description'] ?? null;

    if (empty($desc)) {
        $errors['description'] = 'Beschrijving is verplicht';
    }
    elseif (strlen($desc) > 65000) {
        $errors['description'] = 'Beschrijving is te lang';
    }

    if (empty($price)) {
        $errors['price'] = 'Prijs is verplicht';
    }
    elseif ($price < 0) {
        $errors['price'] = 'Prijs mag niet in de min';
    }
    elseif ($price > 100000) {
        $errors['price'] = 'Prijs mag niet hoger dan honderdduizend';
    }

    if (empty($name)) {
        $errors['name'] = 'Naam is verplicht';
    }
    elseif (strlen($name) > 30) {
        $errors['name'] = 'Naam mag niet langer dan 30 characters';
    }


    if (empty($errors)) {
        if(file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name'])) {
            require 'controllers/upload.php';
            $image = $_FILES['image']['name'];
            $sql = "INSERT into product (name, price, description, image) VALUES (? , ?, ?, ?)";
        } else {
            $sql = "INSERT into product (name, price, description) VALUES (? , ?, ?)";

        }

        $stmt = $pdo->prepare($sql);

        $stmt->execute([
            $name,
            $price,
            $desc,
            $image
        ]);

        header('Location: /products');
    }


}

require 'views/add-product.view.php';
?>
