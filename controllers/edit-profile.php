<?php
  if (empty($_SESSION['loggedin'])) {
    // If they are not, redirect them to the login page.
    header("Location: login");

    // Remember that this die statement is absolutely critical.  Without it,
    // people can view your members-only content without logging in.
    die("Redirecting to login");
}

$pdo = Connection::make($app['config']['database']);
$id = $_SESSION["id"];
$sql = "SELECT * FROM users WHERE user_id = $id";

if ($stmt = $pdo->prepare($sql)) {
    // Attempt to execute the prepared statement
    if ($stmt->execute()) {
        // Check if username exists, if yes then verify password
        if ($stmt->rowCount() == 1) {
            if ($row = $stmt->fetch()) {
                $usernameValue = $row['username'];
                $streetNameValue = $row['streetName'];
                $homeNoValue = $row['homeNo'];
                $postcodeValue = $row['postcode'];
            }

            // Close statement
            unset($stmt);
        }

    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $streetName = $_POST['streetName'] ?? null;
    $homeNo = $_POST['homeNo'] ?? null;
    $username = $_POST['username'] ?? null;
    $postcode = $_POST["postcode"] ?? null;

    if (empty($username)) {
        $errors['username'] = 'Gebruikersaam is verplicht';
    }

    if (empty($streetName)) {
        $errors['streetName'] = 'Straat is verplicht';
    }

    if ($homeNo > 100000) {
        $errors['homeNo'] = 'Huisnummer mag niet hoger dan honderdduizend';
    } elseif ($homeNo < 0) {
        $errors['homeNo'] = 'Huisnummer mag niet in de min';
    }

    if (empty($postcode)) {
        $errors['postcode'] = 'Postcode is verplicht';
    }

    if (empty($errors)) {
        $sqlupdate = "UPDATE users SET username = '$username' , streetName = '$streetName' , homeNo= '$homeNo' , postcode = '$postcode' , updatedAt = NOW() WHERE user_id = $id";
        $stmt = $pdo->prepare($sqlupdate);
        // Attempt to execute the prepared statement
        $stmt->execute();

        // Close statement
        unset($stmt);

        header('Location: profile');
    }
}

require 'views/edit-profile.view.php';
