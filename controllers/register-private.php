<?php
// Include config file
$pdo = Connection::make($app['config']['database']);

// Define variables and initialize with empty values
$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
    } elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["username"]))){
        $username_err = "Username can only contain letters, numbers, and underscores.";
    } else{
        // Prepare a select statement
        $sql = "SELECT user_id FROM users WHERE username = :username";

        if($stmt = $pdo->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":username", $param_username, PDO::PARAM_STR);

            // Set parameters
            $param_username = trim($_POST["username"]);

            // Attempt to execute the prepared statement
            if($stmt->execute()){
                if($stmt->rowCount() == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            unset($stmt);
        }
    }

    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }

    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }

    //Validate firstname
    if(empty(trim($_POST["firstname"]))){
        $firstname_err = "Please fill in first name.";
    }

    //Validate lastname
    if(empty(trim($_POST["lastname"]))){
        $lastname_err = "Please fill in last name.";
    }

    if(empty(trim($_POST["postcode"]))){
        $postcode_err = "Please fill in postcode.";
    }

    if(empty(trim($_POST["streetname"]))){
        $streetname_err = "Please fill in street name.";
    }

    if(empty(trim($_POST["homeNo"]))){
        $homeNo_err = "Please fill in house number.";
    }


    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($firstname_err)
    && empty($lastname_err) && empty($postcode_err) && empty($streetname_err) && empty($homeNo_err)){

        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password, first_name, last_name, postcode, streetName, homeNo, role_id, kvk, btwNumber ) 
                VALUES (:username, :password, :first_name, :last_name, :postcode, :streetName, :homeNo, :roleId, :kvk, :btwNumber)";

        if($stmt = $pdo->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":username", $param_username, PDO::PARAM_STR);
            $stmt->bindParam(":password", $param_password, PDO::PARAM_STR);
            $stmt->bindParam(":first_name", $param_first_name, PDO::PARAM_STR);
            $stmt->bindParam(":last_name", $param_last_name, PDO::PARAM_STR);
            $stmt->bindParam(":postcode", $param_postcode, PDO::PARAM_STR);
            $stmt->bindParam(":streetName", $param_streetname, PDO::PARAM_STR);
            $stmt->bindParam(":homeNo", $param_homeNo, PDO::PARAM_STR);
            $stmt->bindParam(":roleId", $param_roleId, PDO::PARAM_STR);
            $stmt->bindParam(":kvk", $param_kvk, PDO::PARAM_STR);
            $stmt->bindParam(":btwNumber", $param_btwNumber, PDO::PARAM_STR);

            // Set parameters
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            $param_first_name = trim($_POST["firstname"]);
            $param_last_name = trim($_POST["lastname"]);
            $param_postcode = trim($_POST["postcode"]);
            $param_streetname = trim($_POST["streetname"]);
            $param_homeNo = trim($_POST["homeNo"]);
            $param_roleId = (int)$_POST['role'];
            $param_kvk = $_POST['kvk'] ?? null;
            $param_btwNumber = $_POST['btwNumber'] ?? null;
            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // Redirect to login page
                header("location: login");
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            unset($stmt);
        }
    }

    // Close connection
    unset($pdo);
}

require 'views/register-private.view.php';
?>