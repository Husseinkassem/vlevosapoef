<?php
if ($_SESSION['isAdmin'] !== true) {
    header('location: /');
}

$pdo = Connection::make($app['config']['database']);
if(isset($_GET['deleteid'])){
    $id = $_GET['deleteid'];

    $sqlupdate = "DELETE from product WHERE product_id = $id";
    $stmt = $pdo->prepare($sqlupdate);
    // Attempt to execute the prepared statement
    $stmt->execute();

    // Close statement
    unset($stmt);

    header('Location: edit-delete-product');
}
?>