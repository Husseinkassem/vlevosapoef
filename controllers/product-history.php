<?php
// Check if the user is logged in, otherwise redirect to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login");
    exit;
}

$pdo = Connection::make($app['config']['database']);

if (isset($_SESSION['btwNumber'])) {
    if ($_SESSION['btwNumber']) {
        $hasBtw = false;
    } else {
        $hasBtw = true;
    }
} else {
    $hasBtw = true;
}

$user_id = $_SESSION["id"];
$productModel = new Product($pdo);
$invoiceProducts = $productModel->getProductHistory($user_id);

$invoiceModel = new Invoice($pdo);
$invoices = $invoiceModel->showInvoicePerUser($user_id);
$data = [];
foreach ($invoices as $invoice ) {
    $invoiceData = $invoice;
    $invoiceData['products'] = [];
    foreach ($invoiceProducts as $invoiceProduct) {
        if ($invoiceProduct["invoice_id"] === $invoice["invoice_id"]) {
                $invoiceData['products'][] = [
                    'name' => $invoiceProduct['name'],
                    'amount' => $invoiceProduct['amount'],
                    'cost' => $invoiceProduct['cost']
                ];
        }
    }
    $data[] = $invoiceData;

}
require 'views/product-history.view.php';