<?php
// Include config file
$pdo = Connection::make($app['config']['database']);
$hasBtw = !isset($_SESSION['btwNumber']) || $_SESSION['btwNumber'] == '';

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['cartForm'])) {
    $addId = $_POST["addCart"];
    $addName = $_POST["prodName"];
    $image = $_POST["image"];
    $price = (float) $_POST["price"];

    if (empty($_SESSION['cart'])){
        $_SESSION['cart'] = [];
//        $_SESSION['productId'] = array();
//        $_SESSION['aantal'] = array();
//        $_SESSION['productName'] = array();
//        $_SESSION['image'] = array();
//        $_SESSION['price'] = array();
    }

    //Check if $addId exists in $_SESSION['ProductId']
    if (!isset($_SESSION['cart'][$addId])){
        //Since it doesn't exist, add it to the array
        $_SESSION['cart'][$addId] = [
          'productId' => $addId,
          'aantal' => 1,
          'productName' => $addName,
          'image' => $image,
          'price' => $price
        ];
//        array_push($_SESSION['productId'], $addId);
//        array_push($_SESSION['aantal'], 1);
//        array_push($_SESSION['productName'], $addName);
//        array_push($_SESSION['image'], $image);
//        array_push($_SESSION['price'], $price);
    }
    //When it does exist, find the index of the row
    else{
        //Find row
        $_SESSION['cart'][$addId]['aantal']++;
    }

}

$products = [];
$prices = [];
$images = [];
$ids = [];

$minPrice = 0;
$maxPrice = 100;
$name = '';

$model = new Product($pdo);
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['filterForm'])) {
    $minPrice = $_POST['minprice'] ?? null;
    $maxPrice = $_POST['maxprice'] ?? null;
    $name = $_POST['name'] ?? null;

    $sql = $model->showFilter($minPrice, $maxPrice, $name, $hasBtw);
} else {
    $sql = $model->show();
}

while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
    $products[] = [
      'name' => $row['name'],
      'description' => $row['description'],
      'price' => $hasBtw ? number_format($row['price'] * $btw, 2) : number_format($row['price'], 2),
        'image' => $row['image'],
        'id' => $row['product_id']
    ];
}


require 'views/products.view.php';