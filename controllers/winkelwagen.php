<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['cartEdit'])){
        //Qty Edit button was pushed

        //get vars
        $index = $_POST["cartID"];
        $qty = $_POST["cartQty"];

        //Replace old qty with new qty, no need to check if they the same, it aint saving time
        $_SESSION['cart'][$index]['aantal'] = $qty;
    }
    if (isset($_POST['cartRemove'])){
        //get index to be removed
        $index = $_POST["cartID"];
        unset($_SESSION['cart'][$index]);
    }


}

require 'views/winkelwagen.view.php';