<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; }
        .wrapper{ width: 360px; padding: 20px; }
    </style>
</head>
<body>
<?php require 'partials/nav.view.php'?>
<div class="content">
<div class="wrapper">
    <h2>Sign Up</h2>
    <p>Please fill this form to create an account.</p>
    <form method="post">
        <!--Klant role -->
        <input type="hidden" value="2" name="role">
        <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
            <span class="invalid-feedback"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group">
            <label>First name</label>
            <input type="text" name="firstname" class="form-control <?php echo (!empty($firstname_err)) ? 'is-invalid' : ''; ?>">
            <span class="invalid-feedback"><?php echo $firstname_err; ?></span>
        </div>
        <div class="form-group">
            <label>Last name</label>
            <input type="text" name="lastname" class="form-control <?php echo (!empty($lastname_err)) ? 'is-invalid' : ''; ?>" >
            <span class="invalid-feedback"><?php echo $lastname_err; ?></span>
        </div>
        <div class="form-group">
            <label>Postcode</label>
            <input type="text" name="postcode" class="form-control <?php echo (!empty($postcode_err)) ? 'is-invalid' : ''; ?>" >
            <span class="invalid-feedback"><?php echo $postcode_err; ?></span>
        </div>
        <div class="form-group">
            <label>Street</label>
            <input type="text" name="streetname" class="form-control <?php echo (!empty($streetname_err)) ? 'is-invalid' : ''; ?>" >
            <span class="invalid-feedback"><?php echo $streetname_err; ?></span>
        </div>
        <div class="form-group">
            <label>House number</label>
            <input type="number" name="homeNo" class="form-control <?php echo (!empty($homeNo_err)) ? 'is-invalid' : ''; ?>">
            <span class="invalid-feedback"><?php echo $homeNo_err; ?></span>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $password; ?>">
            <span class="invalid-feedback"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group">
            <label>Confirm Password</label>
            <input type="password" name="confirm_password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $confirm_password; ?>">
            <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
        </div>
        <div class="form-group">
            <br>
            <input type="submit" class="btn btn-primary" value="Registreren">
        </div>
        <p>Registreren als bedrijf? <a class="btn btn-primary" href="register-private">Hier registreren</a></p>
        <p>Heb je al een account? <a class="btn btn-primary" href="login">Hier inloggen</a></p>
    </form>
</div>
</div>
<?php require 'partials/footer.view.php'?>
