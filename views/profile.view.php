 <?php require 'partials/header.view.php'?>
 <div class="content">
     <div class="card text-center">
         <h3 class="card-header">
             Profiel gegevens
         </h3>
         <div class="card-body">
             <h5 class="card-text">Gebruikersnaam</h5>
             <p class="card-text"><?php echo $usernameValue ?></p>

             <h5 class="card-text">Volledige naam</h5>
             <p class="card-text"><?php echo $firstnameValue . ' ' . $lastnameValue ?></p>

             <h5 class="card-text">Postcode</h5>
             <p class="card-text"><?php echo $postcodeValue ?></p>

             <h5 class="card-text">Adres</h5>
             <p class="card-text"><?php echo $streetNameValue . ' ' . $homeNoValue ?></p>

         </div>
         <div class="card-footer text-muted">
             <a href="edit-profile" class="btn btn-primary">Bewerk profiel</a>
             <a href="reset-password" class="btn btn-primary">Wachtwoord veranderen</a>
             <a href="product-history" class="btn btn-primary">Aankoop geschiedenis</a>
         </div>
     </div>
 </div>
<?php require 'partials/footer.view.php'?>

