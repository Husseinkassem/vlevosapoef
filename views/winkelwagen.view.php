<?php require 'partials/header.view.php' ?>
<div class="content">
    <h3 style="text-align: center">Producten</h3>
    <div class="row filter-data" style="width: 100%; justify-content: center;
    margin: auto;">
        <div class="row clearfix">
            <?php
            $totalCost = 0;
            //ffs php, waarom kan je 0 niet gewoon 0 laten. Waarom maak je van 0 false?????
            //Naja, dan maar zo he, want 3 is wel een getal toch? En 3 - 3 = 0 en dan pakt ie m wel he?? HE!?
            $index = 0;
            if (!empty($_SESSION['cart'])) {
         ?>
        </div>
    </div>

    <div class="cart_section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="cart_container">
                        <div class="cart_title">Winkelwagen<small> (<?php echo count($_SESSION['cart']) ?> producten)</small></div>
                        <div class="cart_items">
                            <ul class="cart_list">
                                <?php foreach ($_SESSION['cart'] as $product) {
                                    $totalCost += $product['price'] * $product['aantal'];
                                    ?>
                                <li class="cart_item clearfix">
                                    <div class="cart_item_image"><img src="public/uploads/<?php echo $product['image'] ?>"  alt=""></div>
                                    <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                        <div class="cart_item_name cart_info_col">
                                            <div class="cart_item_title">Naam</div>
                                            <div class="cart_item_text"><?php echo $product['productName'] ?></div>
                                        </div>
                                        <div class="cart_item_color cart_info_col">
                                            <div class="cart_item_title">Prijs</div>
                                            <div class="cart_item_text"><?php echo $product['price'] ?></div>
                                        </div>
                                        <div class="cart_item_quantity cart_info_col">
                                            <div class="cart_item_title">Aantal</div>
                                            <div class="cart_item_text">
                                                <form method="post">
                                                    <input type="hidden" name="cartID" value="<?php echo $product['productId'];?>">
                                                    <h5><input class="form-control" type="number" name="cartQty" value="<?php echo $product['aantal'] ?>"></h5>
                                                    <input class="form-control" type="submit" name="cartEdit" value="Edit">
                                                    <input class="form-control" type="submit" name="cartRemove" value="Remove">
                                                </form>
                                            </div>
                                    </div>
                                </li>
                                <?php $index++; } ?>
                            </ul>
                        </div>
                        <div class="order_total">
                            <div class="order_total_content text-md-right">
                                <div class="order_total_title">Totale kosten:</div>
                                <div class="order_total_amount"><?php echo '€' . $totalCost ?></div>
                            </div>
                        </div>
                        <div class="cart_buttons"> <a href="products" class="button cart_button_clear">Door winkelen</a> <a href="checkout" type="button" class="button cart_button_checkout">Bestelling afronden</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
              <?php
                } else { ?>
                <div class="card text-center">
                    <div class="card-header">
                        <h4 style="color: #e53440;">Geen producten in winkelwagen</h4>
                    </div>
                    <div class="card-body">
                        <p>Hier komt al uw producten die u wilt bestellen</p>
                    </div>
                </div>
                  <div class="card text-center">
                      <div class="card-header">
                          <h4 style="color: #e53440;">Hoe voeg je producten toe?</h4>
                      </div>
                      <div class="card-body">
                          <p>Bij de producten pagina kunt u producten toevoegen</p>
                      </div>
                  </div>
                  <div class="card text-center">
                      <div class="card-header">
                          <h4 style="color: #e53440;">Daarna kunt u doorgaan naar de afbetaling</h4>
                      </div>
                      <div class="card-body">
                          <p>En kunt u uw bestelling afronden</p>
                      </div>
                  </div>
                    <?php
                }
            ?>
    </div>
<?php require 'partials/footer.view.php' ?>
<style>

    body {
        font-family: 'Rubik', sans-serif;
        font-size: 14px;
        font-weight: 400;
        background: #E0E0E0;
        color: #000000
    }

    ul {
        list-style: none;
        margin-bottom: 0px
    }

    .button {
        display: inline-block;
        background: #0e8ce4;
        border-radius: 5px;
        height: 48px;
        -webkit-transition: all 200ms ease;
        -moz-transition: all 200ms ease;
        -ms-transition: all 200ms ease;
        -o-transition: all 200ms ease;
        transition: all 200ms ease
    }

    .button a {
        display: block;
        font-size: 18px;
        font-weight: 400;
        line-height: 48px;
        color: #FFFFFF;
        padding-left: 35px;
        padding-right: 35px
    }

    .button:hover {
        opacity: 0.8
    }

    .cart_section {
        width: 100%;
        padding-top: 93px;
        padding-bottom: 111px
    }

    .cart_title {
        font-size: 30px;
        font-weight: 500
    }

    .cart_items {
        margin-top: 8px
    }

    .cart_list {
        border: solid 1px #e8e8e8;
        box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
        background-color: #fff
    }

    .cart_item {
        width: 100%;
        padding: 15px;
        padding-right: 46px
    }

    .cart_item_image {
        width: 133px;
        height: 133px;
        float: left
    }

    .cart_item_image img {
        max-width: 100%
    }

    .cart_item_info {
        width: calc(100% - 133px);
        float: left;
        padding-top: 18px
    }

    .cart_item_name {
        margin-left: 7.53%
    }

    .cart_item_title {
        font-size: 14px;
        font-weight: 400;
        color: rgba(0, 0, 0, 0.5)
    }

    .cart_item_text {
        font-size: 18px;
        margin-top: 35px
    }

    .cart_item_text span {
        display: inline-block;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        margin-right: 11px;
        -webkit-transform: translateY(4px);
        -moz-transform: translateY(4px);
        -ms-transform: translateY(4px);
        -o-transform: translateY(4px);
        transform: translateY(4px)
    }

    .cart_item_price {
        text-align: right
    }

    .cart_item_total {
        text-align: right
    }

    .order_total {
        width: 100%;
        height: 60px;
        margin-top: 30px;
        border: solid 1px #e8e8e8;
        box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
        padding-right: 46px;
        padding-left: 15px;
        background-color: #fff
    }

    .order_total_title {
        display: inline-block;
        font-size: 14px;
        color: rgba(0, 0, 0, 0.5);
        line-height: 60px
    }

    .order_total_amount {
        display: inline-block;
        font-size: 18px;
        font-weight: 500;
        margin-left: 26px;
        line-height: 60px
    }

    .cart_buttons {
        margin-top: 60px;
        text-align: right
    }

    .cart_button_clear {
        display: inline-block;
        border: none;
        font-size: 18px;
        font-weight: 400;
        line-height: 48px;
        color: rgba(0, 0, 0, 0.5);
        background: #FFFFFF;
        border: solid 1px #b2b2b2;
        padding-left: 35px;
        padding-right: 35px;
        outline: none;
        cursor: pointer;
        margin-right: 26px
    }

    .cart_button_clear:hover {
        border-color: #0e8ce4;
        color: #0e8ce4
    }

    .cart_button_checkout {
        display: inline-block;
        border: none;
        font-size: 18px;
        font-weight: 400;
        line-height: 48px;
        color: #FFFFFF;
        padding-left: 35px;
        padding-right: 35px;
        outline: none;
        cursor: pointer;
        vertical-align: top
    }
</style>
