<?php require 'partials/header.view.php'?>
<div class="container-fluid">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-7"><h1>Veelgestelde vragen</h1>
                    <p>
                        <?php
                        foreach ($faq as $item) {
                            echo '<strong>' . $item['q'] . '</strong>' . '<br>';
                            echo $item['a'] . '</br></br>';
                        }
                        ?>
                    </p>
                </div>
                <div class="col"></div>
            </div>

        </div>

    </div>
</div>
<?php require 'partials/footer.view.php'?>
