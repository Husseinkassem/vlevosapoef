<?php require 'partials/nav.view.php'?>
<div class="content">
    <br>
    <?php if (!empty($data)) { ?>
<?php foreach ($data as $item) { ?>
        <?php $totalCost = 0; ?>
<div style="margin-left: 20%; margin-right: 20%" class="card text-center">
    <div class="card-header">
        <h4 style="color: #e53440;">Factuur op <?php echo date_format(date_create($item['createdAt']), 'd-m-Y H:i:s') ?></h4>
    </div>
    <div class="card-body">
        <h5 class="card-title">Producten</h5>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col">Naam</th>
                <th scope="col">Aantal</th>
                <th scope="col">Prijs</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (!empty($item['products'])){
            if (isset($item['products'])) { ?>
    <?php foreach ($item['products'] as $product) {?>
        <?php $totalCost += $product['amount'] * ($product['cost']) ?>
            <tr>
                <td><?php echo $product['name'] ?></td>
                <td><?php echo $product['amount'] ?></td>
                <td>€ <?php echo number_format($product['cost'], 2, '.', '') ?></td>
            </tr>
        <?php } ?>
            <?php }} ?>
            </tbody>
        </table>
        <div class="card-footer text-muted">
         Totale Prijs: € <?php echo number_format($totalCost, 2, '.', '')?> <br>
        </div>
    </div>
</div>
    <br>
<?php } ?>
    <?php } else {
        ?>
    <div style="margin-left: 20%; margin-right: 20%" class="card text-center">
        <div class="card-header">
            <h4 style="color: #e53440;">Geen Facturen</h4>
        </div>
        <div class="card-body">
            <p>Hier komt al uw facturen die u heeft besteld</p>
        </div>
    </div>
    <?php
    }?>
</div>
<?php require 'partials/footer.view.php'?>
