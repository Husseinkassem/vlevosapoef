<?php require 'partials/header.view.php' ?>
<div class="content">
<h3 style="text-align: center">Producten beheer</h3>
<a class="btn btn-primary" href="add-product">Voeg product toe</a>
<br>
<br>
    <div class="table-responsive">
         <table class="table table-hover">
            <tr>
                <th>ID</th>
               <th>plaatje</th>
               <th>naam</th>
               <th>beschrijving</th>
               <th>prijs excl. BTW</th>
               <th>prijs incl. BTW</th>
               <th>bewerken</br>verwijderen</th>
            </tr>
            <?php
               foreach ($products as $product) {
                   ?>
            <tr>
                <th>
                    <?php echo $product['id'] ?>
                </th>
               <th>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                           <img src="/public/uploads/<?php echo $product['image'] ?>" alt="Product" class="img-fluid"
                           width="500"
                           height="500">
               </th>
                <th>
                    <?php echo $product['name'] ?>
                </th>
                <th>
                    <ul class="product_price list-unstyled">
                        <li class="old_price"><?php echo $product['desc'] ?></li>
                    </ul>
                </th>
                <th>
                    <ul class="product_price list-unstyled">
                        <li class="old_price"><?php echo '€' . number_format($product['price'], 2) ?></li>
                    </ul>
                </th>
                <th>
                    <ul class="product_price list-unstyled">
                        <li class="old_price"><?php echo '€' . number_format($product['price'] * $btw, 2) ?></li>
                    </ul>
                </th>
                <th>
                    <div>
                        <a class="btn btn-primary" href="edit-product?updateid=<?php echo $product['id'] ?>">Aanpassen</a>
                        <a class="btn btn-danger" href="delete-product?deleteid=<?php echo $product['id'] ?>">Verwijderen</a>
                    </div>
                </th>
            </tr>
   <?php
      }
      ?>
   </table>
    </div>
</div>
<?php require 'partials/footer.view.php' ?>