<?php require 'partials/header.view.php' ?>
<div class="content">
    <body>
<div class="container mt-5 px-5">
    <div class="mb-4">
        <h2>Order betalen</h2>
    </div>
    <?php
    $totalCost = 0;
    $index = 0;

    foreach ($_SESSION['cart'] as $product) {
        $totalCost += $product['price'] * $product['aantal'];
    }
    ?>
    <div class="row">
        <form method="post">
            <div class="col-md-8">

                <div class="col-md-4">
                    <div class="card card-blue p-3 text-black mb-3"><span>Te betalen bedrag:</span>
                        <div class="d-flex flex-row align-items-end mb-3">
                            <input type="hidden" value="<?php echo $totalCost ?>" name="totalprice">
                            <h1 class="mb-0 yellow"><?php echo $totalCost ?></h1> <span></span>
                        </div>
                    </div>
                </div>
                <div class="card p-3">
                    <!--
                    <h6 class="text-uppercase">Betaal gegevens</h6>
                    <div class="inputbox mt-3"><input type="text" name="name" class="form-control" required="required">
                        <span>Kaart Naam</span></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="inputbox mt-3 mr-2"><input type="text" name="name" class="form-control"
                                                                   required="required"> <i class="fa fa-credit-card"></i>
                                <span>Kaart Nummer</span></div>
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex flex-row">
                                <div class="inputbox mt-3 mr-2"><input type="text" name="name" class="form-control"
                                                                       required="required"> <span>Verloopdatum</span></div>
                                <div class="inputbox mt-3 mr-2"><input type="text" name="name" class="form-control"
                                                                       required="required"> <span>CVV</span></div>
                            </div>
                        </div>
                    </div>
                    -->
                    <div class="mt-4 mb-4">
                        <h6 class="text-uppercase">Aflever Adress</h6>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="inputbox mt-3 mr-2"><span>Postcode</span><input type="text" name="postcode" class="form-control" value="<?php if (isset($_SESSION['id'])){ echo $results['postcode'];} ?>"
                                                                       required="required">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="inputbox mt-3 mr-2"><span>Straat</span><input type="text" name="straat" class="form-control" value ="<?php if (isset($_SESSION['id'])){ echo $results['streetName'];} ?>"
                                                                       required="required">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <div class="inputbox mt-3 mr-2"><span>Huisnummer</span><input type="number" name="homeno" class="form-control" value="<?php if (isset($_SESSION['id'])){ echo $results['homeNo'];} ?>"
                                                                       required="required">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <!--
                                <div class="inputbox mt-3 mr-2"><input type="text" name="postcode" class="form-control"
                                                                       required="required"> <span>Postcode</span>
                                </div>
                                -->
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <div class="mt-3 mr-2"><span>Betaal methoden</span>
                                    <select class="form-control">
                                        <option>Paypal</option>
                                        <option>iDeal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-6">
                                    <div class="mt-3 mr-2"><span>Verzend keuzes</span>
                                        <select class="form-control">
                                            <option>Verzending (1-2 werkdagen)</option>
                                            <option>Ophalen van ophaal punt</option>
                                        </select>
                                    </div>
                                </div>
                            <div class="col-md-6">
                                <!--
                                <div class="inputbox mt-3 mr-2"><input type="text" name="postcode" class="form-control"
                                                                       required="required"> <span>Postcode</span>
                                </div>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4 mb-4 d-flex justify-content-between">
                    <input type="submit" name="pay" class="btn btn-success px-3" value="Bestelling bevestigen">
                </div>
            </div>
        </form>
    </div>
</div>
</div>
<?php require 'partials/footer.view.php' ?>