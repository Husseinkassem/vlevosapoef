<?php require 'views/partials/head.php'?>
<div class="container-fluid">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-7"><h1>Category</h1>
                    <ul class="list-group">
                        <?php
                        foreach ($category as $categor):
                            ?>
                            <li class="list">
                                <h4><?= $categor->title ?></h4>

                            </li>
                        <?php  endforeach;  ?>
                    </ul>
                    </p>
                </div>
                <div class="col"></div>
            </div>

        </div>

    </div>
</div>
<!-- Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>
</html>