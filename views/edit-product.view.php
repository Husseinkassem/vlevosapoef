<?php require 'partials/header.view.php' ?>
<div class="content">
    <div class="wrapper">
    <h1>Product bewerken</h1>
    <br>
    <form method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label>Product naam</label>
        <input type="text" value="<?php echo $name; ?>" name="name" class="form-control <?php echo (!empty($errors['name'])) ? 'is-invalid' : ''; ?>">
        <?php if (isset($errors['name'])) { ?>
            <span class="invalid-feedback"><?php echo $errors['name']; ?></span>
        <?php } ?>
    </div>
    <div class="form-group">
        <label>Prijs</label>
        <input type="number" value="<?php echo $price; ?>" step="any" name="price" class="form-control <?php echo (!empty($errors['price'])) ? 'is-invalid' : ''; ?>">
        <?php if (isset($errors['price'])) { ?>
            <span class="invalid-feedback"><?php echo $errors['price']; ?></span>
        <?php } ?>
    </div>


    <div class="form-group">
        <label>beschrijving</label>
        <br>
        <textarea class="form-control <?php echo (!empty($errors['description'])) ? 'is-invalid' : ''; ?>"
                  name="description" rows="4" cols="50"><?php echo $desc; ?></textarea>
        <?php if (isset($errors['description'])) { ?>
            <span class="invalid-feedback"><?php echo $errors['description']; ?></span>
        <?php } ?>
    </div>
    <div class="form-group">
        <label>Afbeelding</label>
        <input type="file" name="image" class="form-control">
    </div>
    <br>
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-primary" value="Bewerken">
    </div>
    </form>
</div>
</div>
    <style>
        body{ font: 14px sans-serif; }
        .wrapper{ width: 360px; padding: 20px; margin: auto }
    </style>
<?php require 'partials/footer.view.php' ?>