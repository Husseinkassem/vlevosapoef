<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark"
     style="background-image: url('public/img/Flevosap-Footer-achtergrond.png')">
    <img src="public/img/Logo-header-home.png" height="60" width="60">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Flevosap</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link <?=(Request::uri()=== '') ? 'active' :'';?>" aria-current="page" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'products') ? 'active' : 'teachers';?>" href="products">Producten</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'contact') ? 'active' : 'contact';?> " href="contact">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'faq') ? 'active' : 'courses';?> " href="faq">Veelgestelde vragen</a>
                </li>
                <?php
                if (!empty($_SESSION['isAdmin'])) {
                    if ($_SESSION['isAdmin'] === true){ ?>
                        <li class="nav-item">
                            <a class="nav-link  <?=(Request::uri()=== 'edit-delete-product') ? 'active' :'';?>" href="edit-delete-product">Producten beheer</a>
                        </li>
                        <?php
                    }}
                ?>
                <ul class="navbar-nav position-absolute end-0 mx-3">
                    <li class="nav-item">
                        <a class="nav-link" href="winkelwagen">Winkelwagen</a>
                    </li>
                <?php
                if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] === false){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?=(Request::uri()=== 'register') ? 'active' :'';?>" href="register">Registreren</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(Request::uri()=== 'login') ? 'active' :'';?>" href="login">Inloggen</a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?=(Request::uri()=== 'profile') ? 'active' :'';?>" href="profile">Profiel van <?php echo $_SESSION['username'] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="logout">Uitloggen</a>
                    </li>
                    <?php
                }
                ?>
                </ul>
            </ul>
        </div>
    </div>
</nav>
