<!-- Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<footer class="d-flex flex-column bg-dark text-center text-lg-start" style="background-image: url('public/img/Flevosap-Footer-achtergrond.png')">
    <!-- Grid container -->
    <div class="container p-4">
        <!--Grid row-->
        <div class="row">
            <!--Grid column-->

            <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                <img src="public/img/flevosap-light.png" width="150" height="150">
            </div>
            <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                <p class="text-light">
                    Flevosap bv
                    <br>
                    Prof. Zuurlaan 22
                    <br>
                    8256 PE Biddinghuizen, Nederland
                    <br>
                    Tel: +31 (0)321 – 33 25 25
                    <br>
                    info@flevosap.nl</p>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-light text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2021 Copyright:
    </div>
    <!-- Copyright -->
</footer>
<style>
    html, body {
        height: 100%;
    }
    body {
        display: flex;
        flex-direction: column;
    }
    .content {
        flex: 1 0 auto;
    }
    footer {
        flex-shrink: 0;
    }
</style>
</body>
</html>
