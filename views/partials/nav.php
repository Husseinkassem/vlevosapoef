<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet " style="" href="https://cdnjs.cloudflare.com/ajax/libs/event-drops/1.0.2/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- Bootstrap CSS -->
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Flevosap</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link <?=(Request::uri()=== '') ? 'active' :'';?>" aria-current="page" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'products') ? 'active' : 'products';?>" href="products">Producten</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'category') ? 'active' : 'category';?>" href="category">Category</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'contact') ? 'active' : 'contact';?>" href="contact">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= (Request::uri()=== 'faq') ? 'active' : 'FAQ';?> "href="faq">FAQ</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle " href="/" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Log & Register
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="register">Register</a></li>
                        <li><a class="dropdown-item" href="login">Login</a></li>
                        <li><a class="dropdown-item" href="reset-password">Reset password</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="logout">Logout</a></li>
                    </ul>
                 </ul>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>

