 <?php require 'partials/header.view.php'?>
    <div class="content">
        <div class="wrapper">
    <form method="post">
        <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control <?php echo (!empty($errors['username'])) ? 'is-invalid' : ''; ?>" value="<?php echo $usernameValue; ?>">
            <?php if (isset($errors['username'])) { ?>
                <span class="invalid-feedback"><?php echo $errors['username']; ?></span>
            <?php } ?>
        </div>
        <div class="form-group">
            <label>Postcode</label>
            <input type="text" name="postcode" class="form-control <?php echo (!empty($errors['postcode'])) ? 'is-invalid' : ''; ?>" value="<?php echo $postcodeValue ?>">
            <?php if (isset($errors['postcode'])) { ?>
                <span class="invalid-feedback"><?php echo $errors['postcode']; ?></span>
            <?php } ?>
        </div>
        <div class="form-group">
            <label>Straat</label>
            <input type="text" name="streetName" class="form-control <?php echo (!empty($errors['streetName'])) ? 'is-invalid' : ''; ?>" value="<?php echo $streetNameValue ?>">
            <?php if (isset($errors['streetName'])) { ?>
                <span class="invalid-feedback"><?php echo $errors['streetName']; ?></span>
            <?php } ?>
        </div>
        <div class="form-group">
            <label>Huisnummer</label>
            <input type="number" name="homeNo" class="form-control <?php echo (!empty($errors['homeNo'])) ? 'is-invalid' : ''; ?>" value="<?php echo $homeNoValue ?>">
            <?php if (isset($errors['homeNo'])) { ?>
                <span class="invalid-feedback"><?php echo $errors['homeNo']; ?></span>
            <?php } ?>
        </div>
        <div class="form-group">
            <br>
            <input type="submit" class="btn btn-primary" value="Bewerk">
        </div>
    </form>
    </div>

    </div>
 <style>
     body{ font: 14px sans-serif; }
     .wrapper{ width: 360px; padding: 20px; margin: auto }
 </style>
<?php require 'partials/footer.view.php'?>
